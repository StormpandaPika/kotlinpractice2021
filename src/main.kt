val REPEAT_TIMES = 23

fun main() {

    val name = "Austin"
    val age = 33
    val symbol = "%"

    printBorder()
    println("Happy Birthday, ${name}!")
    printBorder(symbol)

    // Let's print a cake!
    println("   ,,,,,   ")
    println("   |||||   ")
    println(" =========")
    println("@@@@@@@@@@@")
    println("{~@~@~@~@~}")
    println("@@@@@@@@@@@")

    // This prints an empty line.
    // Call variable value or variable itself
    println("You are already ${age}!")
    println("$age is the very best age to celebrate!")


}

fun printBorder() {
//    println("=======================")
//    repeat and forEach can not break or continue
    repeat(REPEAT_TIMES) {
        print("=")
    }
    println()

    // Kotlin forEach function test
    val array:IntArray = intArrayOf(1,2,3)
    array.forEach {
        print(it)
    }
    println()
}

fun printBorder(border:String) {
    // Print border symbol for 23 times
    repeat(REPEAT_TIMES) {
        print(border)
    }
    println()
}
